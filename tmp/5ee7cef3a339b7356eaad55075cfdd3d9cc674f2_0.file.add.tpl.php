<?php
/* Smarty version 3.1.32, created on 2018-05-11 20:24:57
  from '/home/tanel/Desktop/hw4/smarty/icd0007/views/add.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5af5d1e90b80b3_64777211',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5ee7cef3a339b7356eaad55075cfdd3d9cc674f2' => 
    array (
      0 => '/home/tanel/Desktop/hw4/smarty/icd0007/views/add.tpl',
      1 => 1526059465,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:form.tpl' => 1,
  ),
),false)) {
function content_5af5d1e90b80b3_64777211 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="et">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="main.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <title>Add</title>
</head>

<body>
<?php $_smarty_tpl->_subTemplateRender("file:header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender("file:form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>

</html><?php }
}
