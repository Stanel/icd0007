<?php

require_once "functions.php";
require_once "contact.php";
require_once('./libs/Smarty.class.php');

$smarty = new Smarty();
$smarty->template_dir = 'views';
$smarty->compile_dir  = 'tmp';

/*
$smarty->error_reporting = E_ALL & ~E_NOTICE;
$smarty->debugging=true;
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

$cmd = isset($_GET["cmd"]) ? $_GET["cmd"] : "view";

if ($cmd == "view") {
    $items = display_contact();
    $smarty->assign('items', $items);
    $smarty->display('index.tpl');

} else if ($cmd == "add") {
    if (isset($_POST["firstName"]) && isset($_POST["lastName"])
        && isset($_POST["phone1"]) && isset($_POST["phone2"]) && isset($_POST["phone3"])){
        $contact = new contact(null, $_POST["firstName"], $_POST["lastName"]);
        $phone1 = $_POST["phone1"];
        $phone2 = $_POST["phone2"];
        $phone3 = $_POST["phone3"];
        add_contact($contact, $phone1, $phone2, $phone3); }
    header("Location: ?cmd=success");
    exit();

} else if ($cmd == "fill") {
    $smarty->display('add.tpl');

} else if ($cmd == "success") {
    $items = display_contact();
    $smarty->assign('items', $items);
    $smarty->display('index.tpl');
}