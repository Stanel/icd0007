<table>
    <thead>
    <tr>
        <th>Eesnimi</th>
        <th>Perekonnanimi</th>
        <th>Telefonid</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$items item="contact"}
        <tr>
            <td>{$contact->firstName}</td>
            <td>{$contact->lastName}</td>
            <td>{$contact->numbers[0]} {$contact->numbers[1]} {$contact->numbers[2]}</td>
        </tr>
    {/foreach}
    </tbody>
</table>